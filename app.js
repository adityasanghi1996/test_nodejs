const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const axios = require('axios');
const app=express();
const PORT=3003;
const GOOGLE_RECAPTCHA_SECRET_SERVER="6Lfb46wUAAAAALmKzBeuU5Co1YZ2zNNaai1VgUiI";
app.use(cors());
app.use(bodyParser.json());

app.post('/captcha/verify',async (req, res)=>{
    const {token}=req.body;
    let secret = GOOGLE_RECAPTCHA_SECRET_SERVER;
    let apiUrl = `https://www.google.com/recaptcha/api/siteverify?secret=${secret}&response=${token}`; 
    
    await axios.get(apiUrl)
    .then(function (response) {
        // handle success
        let result=response && response.data;
        if(result && result.success) {
            res.status(200).json({
                success: result.success,
                timestamp: result.challenge_ts,
                score: result.score
            })
        }
        else {
            if(result && !result.success && result["error-codes"].includes('timeout-or-duplicate'))
            {
                res.status(404).json({
                    errorCode: 404,
                    errorMessage: "Captcha Token Expired",
                    success: false
                })
            }
            res.status(500).json({
              errorCode: 500,
              errorMessage: "Error while verifying captcha",
              success: false
            })
        }
    }).catch(err => {
        res.status(500).json({
            errorCode: 500,
            errorMessage: err.message,
            success: false
        })
    })
})

app.listen(PORT, ()=>{
    console.log("App listening on port "+ PORT)
})